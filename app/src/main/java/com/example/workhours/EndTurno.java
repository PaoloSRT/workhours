package com.example.workhours;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.workhours.DB.DBAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static android.widget.Toast.LENGTH_SHORT;

public class EndTurno extends AppCompatActivity {
    private DBAdapter dbAdapter;
    private DatabaseReference myRef;
    public static double pagaD;
    public static double pagaS;
    public static double paga7;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fine_turno);

        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        final FirebaseDatabase dbFire = FirebaseDatabase.getInstance();
        myRef = dbFire.getReference();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final CheckBox pausaPranzo = (CheckBox) findViewById(R.id.pausa_pr);
        final Button conf = (Button) findViewById(R.id.conf);
        final Button ann = (Button) findViewById(R.id.ann);

        Main.pref = getSharedPreferences("user_pref", MODE_PRIVATE);
        final String d = Main.pref.getString("paga_Domenica", "");
        final String s = Main.pref.getString("paga_Sabato", "");
        final String f = Main.pref.getString("paga_feriale", "");

        if(d.isEmpty())
            pagaD = 9.00;
        else
            pagaD = Double.parseDouble(d);
        if(s.isEmpty())
            pagaS = 7.50;
        else
            pagaS = Double.parseDouble(s);
        if(f.isEmpty())
            paga7 = 7.50;
        else
            paga7 = Double.parseDouble(f);

        final String turnoIniziato = getIntent().getExtras().getString("turno");
        //String turnoIniziato = "0 Saturday 26-12-2020 09:39 null null 0.0 0.0 0.0";

        dbAdapter.cancellaTurnoNC();

        final Turno turnoCompleto = new Turno();
        final Turno turnoPausa = new Turno();

        //Divisione dati String passata da altra activity
        String[] propTurno =  turnoIniziato.split(" ");
        turnoCompleto.setGSett(propTurno[1]);
        turnoCompleto.setDataIni(propTurno[2]);
        turnoCompleto.setStart(propTurno[3]);

        //Riempimento oggetto Turno
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String fine = formatter.format(date);
        String[] propFineTurno = fine.split(" ");
        turnoCompleto.setDataFin(propFineTurno[0]);
        turnoCompleto.setEnd(propFineTurno[1]);

        final GregorianCalendar start = Turno.creaCalendarStart(turnoCompleto);
        final GregorianCalendar end = Turno.creaCalendarEnd(turnoCompleto);
        final long endMilli = end.getTimeInMillis();
        final long startMilli = start.getTimeInMillis();
        Turno.differenzaOre(turnoCompleto, start, end);
        riempiTextView(turnoCompleto);

        //Controllo turno maggiore di 30 minuti per selezionare pausa pranzo (pari a 30 minuti)
        if((endMilli-startMilli)<1800000){
            pausaPranzo.setClickable(false);
            Toast.makeText(EndTurno.this, "Turno inferiore a 30 minuti: pausa non selezionabile!", LENGTH_SHORT).show();
        } else {
            turnoPausa.addPausa(turnoCompleto, start, end);
            pausaPranzo.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (pausaPranzo.isChecked()) {
                        riempiTextView(turnoPausa);
                        Toast.makeText(EndTurno.this, "Pausa pranzo aggiunta! ", LENGTH_SHORT).show();
                    } else {
                        riempiTextView(turnoCompleto);
                    }
                }
            });
        }

        conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Aggiunta Turno al DB
                DBAdapter db = DBAdapter.getInstance(EndTurno.this);
                db.open();
                long idLastTurno;
                myRef = dbFire.getReference().child("users").child("id " + Main.userID).child("turni");
                if (pausaPranzo.isChecked()) {
                    db.addTurno(turnoPausa, Main.userID);
                    idLastTurno = db.lastIDTurno(Main.userID);
                    HashMap<String, Object> tMap = new HashMap<>();
                    tMap.put("id_turno " + idLastTurno, turnoPausa);
                    myRef.updateChildren(tMap);
                } else {
                    db.addTurno(turnoCompleto, Main.userID);
                    idLastTurno = db.lastIDTurno(Main.userID);
                    HashMap<String, Object> tMap = new HashMap<>();
                    tMap.put("id_turno " + idLastTurno, turnoCompleto);
                    myRef.updateChildren(tMap);
                }
                db.close();

                Intent home = new Intent(EndTurno.this, Main.class);
                startActivity(home);
                Toast.makeText(EndTurno.this, "Turno aggiunto con successo!", LENGTH_SHORT).show();
                finish();
            }
        });

        ann.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EndTurno.this);;
                builder.setMessage("Sicuro di cancellare questo turno?");
                builder.setTitle("Cancella");
                builder.setPositiveButton("Cancella!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent home = new Intent(EndTurno.this, Main.class);
                        startActivity(home);
                        finish();
                    }
                });
                builder.setNegativeButton("Annulla", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    //Metodo per riempire i campi dell'activity
    public void riempiTextView(Turno t){
        final TextView oraStart = (TextView) findViewById(R.id.ora_in);
        final TextView oraFine = (TextView) findViewById(R.id.ora_fine);
        final TextView totOre = (TextView) findViewById(R.id.ora_tot);
        final TextView totText = (TextView) findViewById(R.id.tot);

        oraStart.setText(t.getStart());
        oraFine.setText(t.getEnd());
        totOre.setText(String.valueOf(t.getTOra()));
        totText.setText(String.valueOf(t.getTotale()));
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Navigazione indietro disattivata per evitare di non salvare il turno!", LENGTH_SHORT).show();
    }
}
