package com.example.workhours;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    protected EditText emailEditText;
    protected EditText passwordEditText;
    protected Button logInButton;
    protected Button regButton;
    protected Button logOutButton;
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        emailEditText = findViewById(R.id.user_email);
        passwordEditText = findViewById(R.id.user_password);
        logInButton = findViewById(R.id.button_log);
        regButton = findViewById(R.id.button_reg);
        logOutButton = findViewById(R.id.button_out);

        try{
            mFirebaseUser = mAuth.getCurrentUser();
            String user = mFirebaseUser.getEmail();
            emailEditText.setText(user);
            logInButton.setEnabled(false);
            regButton.setEnabled(false);
        } catch(RuntimeException ex){
            emailEditText.setHint("Inserisci Email");
            logOutButton.setEnabled(false);
        }

        //Login utente gia' registrato
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                email = email.trim();
                password = password.trim();
                //Controllo presenza stringhe
                if (email.isEmpty() || password.isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);;
                    builder.setMessage(R.string.email_pass_mancante);
                    builder.setTitle("Errore");
                    builder.setNegativeButton("Ok", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        Toast.makeText(Login.this, "Accesso effettuato", Toast.LENGTH_SHORT)
                                                .show();
                                        Intent intent = new Intent(Login.this, Main.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);;
                                        builder.setMessage(task.getException().getMessage());
                                        builder.setTitle("Errore");
                                        builder.setNegativeButton("Ok", null);
                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }
                                }
                            });
                }
            }
        });

        //Registrazione nuovo utente
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                email = email.trim();
                password = password.trim();
                if (email.isEmpty() || password.isEmpty()) { //controllo presenza stringhe
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setMessage(R.string.email_pass_mancante);
                    builder.setTitle("Errore");
                    builder.setNegativeButton("Ok", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Intent intent = new Intent(Login.this, Login.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                                        builder.setMessage(task.getException().getMessage());
                                        builder.setTitle("Errore");
                                        builder.setNegativeButton("Ok", null);
                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                    }
                                }
                            });
                }
            }
        });

        //Logout dell'utente
        logOutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mAuth.signOut();
                Intent home = new Intent(Login.this, Main.class);
                startActivity(home);
                finish();
            }
        });
    }
}
