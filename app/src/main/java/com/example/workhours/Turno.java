package com.example.workhours;

import android.util.Log;

import com.google.firebase.database.Exclude;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class Turno {
    private int id;
    private String dataIni, dataFin, gSett, start, end;
    private double tOra, pOra, totale;
    public Turno(){}

    public Turno(Turno t){
        super();
    }

    public int getId(){
        return this.id;
    }

    public String getDataIni(){
        return this.dataIni;
    }

    public String getDataFin(){
        return this.dataFin;
    }

    public String getGSett(){
        return this.gSett;
    }

    public String getStart(){
        return this.start;
    }

    public String getEnd(){
        return this.end;
    }

    public double getTOra(){
        return this.tOra;
    }

    public double getPOra(){
        return this.pOra;
    }

    public double getTotale(){
        return this.totale;
    }

    public void setDataIni(String data){
        this.dataIni = data;
    }

    public void setDataFin(String data){
        this.dataFin = data;
    }

    public void setGSett(String gSett){
        this.gSett = gSett;
    }

    public void setStart(String start){
        this.start = start;
    }

    public void setEnd(String end){
        this.end = end;
    }

    public void setTOra(double totOre){
        this.tOra = totOre;
    }

    public void setPOra(double paga){
        this.pOra = paga;
    }

    public void setTotale(double TOT){
        this.totale = TOT;
    }

    //Utilizzato GregorianCalendar al posto di Calendar per l'ottimizzazione di roll() che si propaga anche negli altri campi
    public static GregorianCalendar creaCalendarStart(Turno t) {
        String[] propOra = t.getStart().split(":");
        String[] propData = t.getDataIni().split("-");
        GregorianCalendar s = new GregorianCalendar(Integer.parseInt(propData[2]), Integer.parseInt(propData[1]),
                Integer.parseInt(propData[0]), Integer.parseInt(propOra[0]), Integer.parseInt(propOra[1]));
        return s;
    }

    public static GregorianCalendar creaCalendarEnd(Turno t) {
        String[] propOraF = t.getEnd().split(":");
        String[] propDataF = t.getDataFin().split("-");
        //Calendar f = Calendar.getInstance();
        GregorianCalendar f = new GregorianCalendar(Integer.parseInt(propDataF[2]), Integer.parseInt(propDataF[1]),
                Integer.parseInt(propDataF[0]), Integer.parseInt(propOraF[0]), Integer.parseInt(propOraF[1]));
        return f;
    }

    public void addPausa(Turno t, GregorianCalendar s, GregorianCalendar f){
        this.setDataIni(t.getDataIni());
        this.setGSett(t.getGSett());
        this.setEnd(t.getEnd());
        s.add(GregorianCalendar.MINUTE, 30);
        String ora = String.format("%02d", s.get(GregorianCalendar.HOUR_OF_DAY));
        String minuti = String.format("%02d", s.get(GregorianCalendar.MINUTE));
        String oraCorretta = ora + ":" + minuti;
        this.setStart(oraCorretta);
        differenzaOre(this, s, f);
    }

    public static void differenzaOre(Turno t, GregorianCalendar s, GregorianCalendar f) {
        //Calcolo durata turno
        final int numGiornoSett = s.get(GregorianCalendar.DAY_OF_WEEK);
        long oraini = s.getTimeInMillis();
        long orafin = f.getTimeInMillis();
        double diff = orafin - oraini;
        double tot = diff / (60 * 60 * 1000);
        tot = Math.floor(tot * 100);
        t.setTOra(tot / 100);

        //Paga oraria in base al giorno della settimana
        switch(numGiornoSett){
            case 4: //Domenica
                t.setPOra(EndTurno.pagaD);
                break;
            case 3: //Sabato
                t.setPOra(EndTurno.pagaS);
                break;
            default: //Resto della settimana
                t.setPOra(EndTurno.paga7);
                break;
        }

        //Guadagno totale del turno
        double TOT = t.getTOra()*t.getPOra();
        TOT = Math.floor(TOT*100);
        t.setTotale(TOT/100);

    }

    @Override
    public String toString(){
        return "" + id + " " + gSett + " " + dataIni  + " " + start  + " " + dataFin + " " + end + " "
                + tOra + " " + pOra + " " + totale;
    }

    public String toStringLong() {
        return "Giorno = " + gSett + "\n"
                +"data = " + dataIni + "\n"
                +"ora inizio = " + start + "\n"
                +"ora fine = " + end + "\n"
                +"totale ore = " + tOra + "\n"
                +"paga oraria = " + pOra + "\n"
                +"TOTALE = €" + totale;
    }

    public String toList(){
        return dataIni + " : " + start + " ► " + end;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("giorno", gSett);
        result.put("data", dataIni);
        result.put("ora_inizio", start);
        result.put("ora_fine", end);
        result.put("tot_ore", tOra);
        result.put("paga", pOra);
        result.put("tot", totale);
        result.put("user", Main.userID);

        return result;
    }
}
