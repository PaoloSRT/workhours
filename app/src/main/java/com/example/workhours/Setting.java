package com.example.workhours;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Setting extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Modifica e salvataggio della paga oraria
        String val;
        final EditText textDom = (EditText) findViewById(R.id.paga_dom);
        textDom.setText(Main.pref.getString("paga_Domenica", ""));
        final EditText textSab = (EditText) findViewById(R.id.paga_sab);
        textSab.setText(Main.pref.getString("paga_Sabato", ""));
        final EditText textFer = (EditText) findViewById(R.id.paga_fer);
        textFer.setText(Main.pref.getString("paga_feriale", ""));

        FloatingActionButton set = (FloatingActionButton) findViewById(R.id.set);
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Main.ed = Main.pref.edit();
                Main.ed.putString("paga_Domenica", textDom.getText().toString());
                Main.ed.putString("paga_Sabato", textSab.getText().toString());
                Main.ed.putString("paga_feriale", textFer.getText().toString());
                Main.ed.commit();
                Toast.makeText(Setting.this, "Modifica paga oraria salvata!", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }
}
