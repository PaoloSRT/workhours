package com.example.workhours.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.workhours.Turno;

public class DBAdapter {
    private static DBAdapter adapInstance;
    private SQLiteDatabase db = null;
    private DBHelperTurni dbHelper;

    private static final String TABLE_TURNI = "turni";
    private static final String TABLE_TURNI_NC = "turno_nc";
    private static final String KEY_NUM = "ID_Turno";
    private static final String KEY_UID = "ID_User";
    private static final String KEY_DATAINI = "data_inizio";
    private static final String KEY_GIORNO = "giorno_settimana";
    private static final String KEY_ST_ORA = "ora_inizio";
    private static final String KEY_END_ORA = "ora_fine";
    private static final String KEY_TOT_ORA = "totale_ore";
    private static final String KEY_PAGA_ORA = "paga_ora";
    private static final String KEY_TOT = "totale";
    private static final String[] COLONNE = {KEY_NUM, KEY_UID, KEY_GIORNO, KEY_DATAINI, KEY_ST_ORA, KEY_END_ORA, KEY_TOT_ORA, KEY_PAGA_ORA, KEY_TOT};

    public static synchronized DBAdapter getInstance(Context context){
      if(adapInstance == null)
          adapInstance = new DBAdapter(context.getApplicationContext());
      return adapInstance;
    }

    private DBAdapter(Context context){
        this.dbHelper = DBHelperTurni.getInstance(context);
    }

    public DBAdapter open() throws SQLException{
        if(db != null) return this;
        try {
            db = dbHelper.getWritableDatabase();
        } catch(SQLException e) {
            Log.e("DBADAPTER", e.getMessage());
            throw e;
        }
        return this;
    }

    public void close() {
        if(db == null) return;
        db.close();
        db = null;
    }

    public void addTurno(Turno t, String id){
        ContentValues values = new ContentValues();
        values.put(KEY_GIORNO, t.getGSett());
        values.put(KEY_DATAINI, t.getDataIni());
        values.put(KEY_ST_ORA, t.getStart());
        values.put(KEY_END_ORA, t.getEnd());
        values.put(KEY_TOT_ORA, t.getTOra());
        values.put(KEY_PAGA_ORA, t.getPOra());
        values.put(KEY_TOT, t.getTotale());
        values.put(KEY_UID, id);

        db.insert(
                TABLE_TURNI,
                null,
                values
        );

        Log.d("TURNO INSERITO", t.toStringLong() + "\nUser id:" + id);
    }

    //Metodo per ritornare un Turno partendo dal suo ID_Turno
    public Turno showTurno(long id){
        Turno turno = new Turno();
        Cursor c = db.rawQuery("select * from turni where ID_Turno = ?", new String[] {String.valueOf(id)});
        c.moveToFirst();
        turno.setGSett(c.getString(2));
        turno.setDataIni(c.getString(3));
        turno.setStart(c.getString(4));
        turno.setEnd(c.getString(5));
        turno.setTOra(Double.parseDouble(c.getString(6)));
        turno.setPOra(Double.parseDouble(c.getString(7)));
        turno.setTotale(Double.parseDouble(c.getString(8)));
        return turno;
    }

    public long lastIDTurno(String user){
        Cursor c = db.rawQuery("select count(*) from turni where ID_User = ?", new String[] {user});
        c.moveToFirst();
        long idTurno = c.getInt(0);
        Log.d("\t Num turno inserito", String.valueOf(idTurno));
        return idTurno;
    }

    //Metodo per visualizzare tutti i turni di un utente
    public Cursor showAllTurni(String id){
        return db.rawQuery(" select ID_Turno, data_inizio, ora_inizio, ora_fine, totale_ore " +
                "from turni " +
                "where ID_User = ?", new String[]{id});
    }

    //Metodo per visualizzare i turni di uno specifico mese di un utente
    public Cursor showTurniMese(String anno, String mese, String id){
        return db.rawQuery(" select ID_Turno, data_inizio, ora_inizio, ora_fine, totale_ore " +
                "from turni " +
                "where substr(data_inizio, 4, 2) = ?  and " +
                "substr(data_inizio, 7, 4) = ?  and " +
                "ID_User = ?", new String[] {mese, anno, id});
    }

    //Metodo per estrarre il totale guadagnato in uno specifico mese
    public Double totMeseSel(String anno, String mese, String id){
        Cursor c = db.rawQuery(" select SUM(totale) " +
                "from turni " +
                "where substr(data_inizio, 4, 2) = ?  and " +
                "substr(data_inizio, 7, 4) = ?  and " +
                "ID_User = ?", new String[] {mese, anno, id});

        Double tot = null;
        if(c.moveToFirst()){
            tot = c.getDouble(0);
        }
        return tot;
    }

    //Metodo per cancellare un turno dal DB
    public boolean cancellaTurno(long id){
        return db.delete(TABLE_TURNI, KEY_NUM + "=" + id, null) > 0;
    }

    public void addTurnoNC(Turno t, String id){
        ContentValues values = new ContentValues();
        values.put(KEY_GIORNO, t.getGSett());
        values.put(KEY_DATAINI, t.getDataIni());
        values.put(KEY_ST_ORA, t.getStart());
        values.put(KEY_UID, id);

        db.insert(
                TABLE_TURNI_NC,
                null,
                values
        );

        db.close();
    }

    public Turno getTurnoNC(){
        Turno turnoNC = new Turno();
        Cursor c = db.rawQuery("select * from turno_nc ", null);
        c.moveToFirst();
        turnoNC.setGSett(c.getString(2));
        turnoNC.setDataIni(c.getString(3));
        turnoNC.setStart(c.getString(4));
        return turnoNC;
    }

    public boolean turnoNC(){
        Cursor cCount = db.rawQuery("select * " +
                "from turno_nc ", null);
        if(cCount.getCount() == 0)
            return false;
        else
            return true;
    }

    public boolean cancellaTurnoNC(){
        return db.delete(TABLE_TURNI_NC, null, null) > 0;
    }
}
