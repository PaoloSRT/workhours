package com.example.workhours.DB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelperTurni extends SQLiteOpenHelper {
    private static DBHelperTurni instance;
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "TurniDB";

    public static synchronized DBHelperTurni getInstance(Context context){
        if(instance==null){
            instance = new DBHelperTurni(context.getApplicationContext());
        }
        return instance;
    }

    private DBHelperTurni(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) throws SQLException{
        String CREATE_TURNI_TABLE =
                "CREATE TABLE turni (" +
                        "ID_Turno INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "ID_User TEXT, " +
                        "giorno_settimana TEXT, " +
                        "data_inizio DATE, " +
                        "ora_inizio DATE, " +
                        "ora_fine DATE, " +
                        "totale_ore DATE, " +
                        "paga_ora REAL, " +
                        "totale REAL ) " ;

        //
        String CREATE_TURNONC_TABLE =
                "CREATE TABLE turno_nc (" +
                        "ID_Turno INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "ID_User TEXT, " +
                        "giorno_settimana TEXT, " +
                        "data_inizio DATE, " +
                        "ora_inizio DATE ) ";

        try{
            db.execSQL(CREATE_TURNI_TABLE);
            db.execSQL(CREATE_TURNONC_TABLE);
        } catch(SQLException e){
            Log.e("DBHELPER", e.getMessage());
            throw e;
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVers, int newVers) {
        db.execSQL("DROP TABLE IF EXISTS turni");
        db.execSQL("DROP TABLE IF EXISTS turno_nc");
        this.onCreate(db);
    }
}
