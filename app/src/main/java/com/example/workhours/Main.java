package com.example.workhours;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.workhours.DB.DBAdapter;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class Main extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static String user;
    private DrawerLayout drawer;
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private DBAdapter dbAdapter;
    private boolean userLog;
    protected static SharedPreferences pref;
    protected static SharedPreferences.Editor ed;
    protected static String userID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawer = findViewById(R.id.drawer);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View nView = navigationView.getHeaderView(0);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Controllo esistenza turno non concluso
        if(dbAdapter.turnoNC()){
            Toast.makeText(this, "Turno in sospeso non concluso", Toast.LENGTH_SHORT).show();
            Turno turnoNonConc = dbAdapter.getTurnoNC();
            Intent continuaTurno = new Intent(Main.this, StartTurno.class);
            continuaTurno.putExtra("turno_sosp", turnoNonConc.toString());
            startActivity(continuaTurno);
            finish();
        }

        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mAuth.getCurrentUser();
        TextView txtUser = (TextView) nView.findViewById(R.id.text_user);

        try {
            String[] tmp = mFirebaseUser.getEmail().split("@");
            user = tmp[0];
            txtUser.setText(user);
            userLog = true;
            userID = mFirebaseUser.getUid();
            Log.d("\t Utente loggato", user);
        } catch(RuntimeException re){
            Log.d("\t Nessun utente loggato", "effettuare login");
            userLog = false;
        }

        pref = getSharedPreferences("user_pref", MODE_PRIVATE);

        Button start = (Button) findViewById(R.id.start_turno);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Controllo sul login di almeno un utente prima di avviare un turno
                if(userLog) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Main.this);
                    builder.setMessage("Inizio del turno? ");
                    builder.setTitle("Conferma");
                    builder.setPositiveButton("Inizia", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intTurno = new Intent(Main.this, StartTurno.class);
                            startActivity(intTurno);
                            finish();
                        }
                    });
                    builder.setNegativeButton("Annulla", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    //Passaggio diretto all'activity di login
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(Main.this);
                    builder1.setMessage("Impossibile iniziare un turno finché non viene autenticato un utente!");
                    builder1.setTitle("Errore");
                    builder1.setPositiveButton("Vai al LogIn", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent logRapido = new Intent(Main.this, Login.class);
                            startActivity(logRapido);
                        }
                    });
                    AlertDialog dialog = builder1.create();
                    dialog.show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch(menuItem.getItemId()){
            case R.id.login :
                Intent intLog = new Intent(this, Login.class);
                startActivity(intLog);
                break;
            case R.id.storico :
                if(userLog) {
                    Intent intSto = new Intent(this, ListaTurni.class);
                    startActivity(intSto);
                } else {
                    Toast.makeText(this, "Autenticare un utente prima di procedere", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.setting :
                Intent intSet = new Intent(this, Setting.class);
                startActivity(intSet);
                break;
            default:
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
