package com.example.workhours;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.workhours.DB.DBAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StartTurno extends AppCompatActivity {
    private DBAdapter dbAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.turno_activity);

        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Button fine = (Button) findViewById(R.id.button_fine);
        final Turno newTurno = new Turno();
        final String strTurno;

        //Controllo esistenza di un turno non concluso
        if(dbAdapter.turnoNC()){
            strTurno = getIntent().getExtras().getString("turno_sosp");
        } else {
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd-MM-yyyy HH:mm");
            String inizio = formatter.format(date);
            String[] propTurno = inizio.split(" ");
            newTurno.setGSett(propTurno[0]);
            newTurno.setDataIni(propTurno[1]);
            newTurno.setStart(propTurno[2]);
            strTurno = newTurno.toString();

            DBAdapter db = DBAdapter.getInstance(StartTurno.this);
            db.open();
            db.addTurnoNC(newTurno, Main.userID);
            db.close();
        }

        fine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(StartTurno.this);;
                builder.setMessage("Fine del turno? \nNon sarà più possibile continuarlo!");
                builder.setTitle("Conferma");
                builder.setPositiveButton("Concludi", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intFineTurno = new Intent(StartTurno.this, EndTurno.class);
                        intFineTurno.putExtra("turno", strTurno);
                        startActivity(intFineTurno);
                        finish();
                    }
                });
                builder.setNegativeButton("Annulla", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }
}
