package com.example.workhours;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.example.workhours.DB.DBAdapter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListaTurni extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {
    protected static ArrayAdapter ad;
    protected String[] turniList;
    private DBAdapter dbAdapter;
    private ListView list;
    private ArrayList<String> turni;
    private Button cercaMese ;
    private TextView labelTotMese;
    private TextView totMese;
    private Cursor c;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setOnMenuItemClickListener(this);

        list = (ListView) findViewById(R.id.list_view);
        turni = new ArrayList<String>();
        ad = new ArrayAdapter<String>(ListaTurni.this, android.R.layout.simple_list_item_1, turni);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        cercaMese = (Button) findViewById(R.id.cerca_mese);
        labelTotMese = (TextView) findViewById(R.id.label_totale_mese);
        totMese = (TextView) findViewById(R.id.tot_mese);
        cercaMese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonthYearPicker picker = new MonthYearPicker();
                picker.show(getSupportFragmentManager(), "MonthYearPickerDialog");
                picker.setOnMonthSelectedListener(new MonthYearPicker.IMonthYearPicker()
                {
                    @Override
                    public void OnMonthSelected()
                    {
                        loadData();
                    }
                });
            }
        });

        loadData();

        final Cursor finalC = c;
        //Informazioni complete del turno
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                DBAdapter dbAdapter = DBAdapter.getInstance(ListaTurni.this);
                dbAdapter.open();
                finalC.moveToPosition(i);
                long pos = finalC.getLong(finalC.getColumnIndex("ID_Turno"));
                Turno t = dbAdapter.showTurno((int) pos);
                Toast.makeText(ListaTurni.this, "Info: " + t.toStringLong(), Toast.LENGTH_SHORT).show();
                dbAdapter.close();
            }
        });

        //Cancellazione turno
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaTurni.this);;
                builder.setMessage("Cancellare questo turno?");
                builder.setTitle("Conferma");
                builder.setPositiveButton("Cancella", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DBAdapter dbAdapter = DBAdapter.getInstance(ListaTurni.this);
                        dbAdapter.open();
                        finalC.moveToPosition(i);
                        long pos = finalC.getLong(finalC.getColumnIndex("ID_Turno"));
                        dbAdapter.cancellaTurno(pos);
                        turni.remove(i);
                        ad.notifyDataSetChanged();
                        loadData();
                        dbAdapter.close();
                    }
                });
                builder.setNegativeButton("Annulla", null);
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });

        dbAdapter.close();
    }

    //Popolamento listview
    public void loadData() {
        try {
            dbAdapter.open();
            c = dbAdapter.showTurniMese(MonthYearPicker.cercaAnno, MonthYearPicker.cercaMese, Main.userID);
            if(c.getCount()==0) {
                labelTotMese.setVisibility(View.INVISIBLE);
                totMese.setVisibility(View.INVISIBLE);
                Toast.makeText(this, "Nessun turno registrato per il mese selezionato", Toast.LENGTH_SHORT).show();
                ad.notifyDataSetChanged();
            } else {
                labelTotMese.setVisibility(View.VISIBLE);
                totMese.setVisibility(View.VISIBLE);
                totMese.setText("€ " + dbAdapter.totMeseSel(MonthYearPicker.cercaAnno, MonthYearPicker.cercaMese, Main.userID));
                ad.notifyDataSetChanged();
            }
        } catch(Exception ex) {
            labelTotMese.setVisibility(View.INVISIBLE);
            totMese.setVisibility(View.INVISIBLE);
            c = dbAdapter.showAllTurni(Main.userID);
            ad.notifyDataSetChanged();
        }

        list.setAdapter(ad);
        turniList = new String[c.getCount()];
        turni.clear();
        if(c.moveToFirst()) {
            int i = 0;
            do {
                Turno t = new Turno();
                t.setDataIni(c.getString(1));
                t.setStart(c.getString(2));
                t.setEnd(c.getString(3));
                t.setTOra(Double.parseDouble(c.getString(4)));
                turni.add(t.toList() + "\n" + t.getTOra() + " ore");
                turniList[i] = t.toList() + "\n";
                i++;
            } while (c.moveToNext());
        }
        ad.notifyDataSetChanged();
        dbAdapter.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.storico_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem){
        switch(menuItem.getItemId()) {
            //Creazione e condivisione di un file .txt con l'elenco dei turni del mese selezionato
            case R.id.condividi:
                File file;
                if(MonthYearPicker.cercaAnno == null & MonthYearPicker.cercaMese == null){
                    Toast.makeText(this, "Selezionare un mese", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        file = File.createTempFile("turni-mese-" + MonthYearPicker.cercaMese + "-" + MonthYearPicker.cercaAnno + "_", ".txt");
                        Log.d("\t Locazione file", file.getPath());
                        FileWriter fw = new FileWriter(file);
                        fw.write("File con i turni del mese " + MonthYearPicker.cercaMese + "/" + MonthYearPicker.cercaAnno + " dell'utente " + Main.user +
                                "\n\n");
                        for (int i = 0; i < turniList.length; i++) {
                            fw.write(turniList[i]);
                        }
                        fw.flush();
                        fw.close();

                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("Documents/*");
                        Uri uri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                                BuildConfig.APPLICATION_ID + ".provider", file);
                        String shareSub = "File .txt con i turni del mese " + MonthYearPicker.cercaMese + "/" + MonthYearPicker.cercaAnno;
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        Intent chooser = Intent.createChooser(shareIntent, "Share File");

                        //Garantire i permessi di scrittura e lettura
                        List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY);
                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            this.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }

                        startActivity(chooser); //Errore generato da startActivity(Intent.createChooser(shareIntent, "Share App Locker"));
                    } catch (IOException e) {
                        Toast.makeText(this, "Errore nella creazione del file", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    } catch (SecurityException s) {
                        s.printStackTrace();
                    }
                    return true;
                }

            //Revoca dei filtri della ricerca
            case R.id.reset:
                MonthYearPicker.cercaAnno = null;
                MonthYearPicker.cercaMese = null;
                loadData();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        MonthYearPicker.cercaAnno = null;
        MonthYearPicker.cercaMese = null;
        finish();
        super.onBackPressed();
    }
}
