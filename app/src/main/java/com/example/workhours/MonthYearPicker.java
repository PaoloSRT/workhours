package com.example.workhours;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MonthYearPicker extends DialogFragment {

    public interface IMonthYearPicker {
        public void OnMonthSelected();
    }

    private static final int MIN_YEAR = 2016;
    private static final int MAX_YEAR = 2050;
    private IMonthYearPicker myListener = null;
    protected static String cercaMese;
    protected static String cercaAnno;

    public MonthYearPicker() {}

    public void setOnMonthSelectedListener(IMonthYearPicker listener)
    {
        myListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        GregorianCalendar today = (GregorianCalendar) GregorianCalendar.getInstance();

        final View dialog = inflater.inflate(R.layout.datepicker_dialog, null);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        //Creazione dei NumberPicker e impostazione sul mese e anno corrente
        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(today.get(Calendar.MONTH) + 1);

        yearPicker.setMinValue(MIN_YEAR);
        yearPicker.setMaxValue(MAX_YEAR);
        yearPicker.setValue(today.get(Calendar.YEAR));

        builder.setView(dialog)
                .setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //String.format usato per evitare venisse cancellato lo 0 iniziale davanti ai mesi com Gennaio (01)
                        cercaMese = String.format("%02d", monthPicker.getValue());
                        cercaAnno = String.valueOf(yearPicker.getValue());

                        if(myListener != null) myListener.OnMonthSelected();
                    }
                })
                .setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MonthYearPicker.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
}
